iframe message-passing proxy example
=================================

## Purpose
This repo shows a simple server for which for each request `/$foo`, the same outer frame is served along with an inner frame that loads `/proxy/$foo`.

Requests to `/static/$foo` are an exception. These are files are served from `/public/$foo` in the server's file system.

For the purpose of the demonstration we're pretending the `html` files here are content from another server, and we can't or don't want to modify them directly.

`/static/js` contains files which implement message-passing between frames. These would live on the same server that hosts the outer frame template. 

`/proxy/$foo` makes a request to `/static/$foo`, again pretending this is 3rd-party content. If the result is HTML, it does a simple search-and-replace to inject `<script>` tags that load the mechanism necessary for the inner frame to tell the outer frame its title and location.

The end result of all that is that UI content of the outer frame is kept consistent with the inner frame.

Navigating to a new page in the inner frame will update the navigation bar, so that things like bookmarking, refreshing, link sharing, ,and back-forward on the outer frame are reflected properly in the inner frame.

## Installation & Running
  1. git clone
  2. npm install
  3. npm start
  4. Visit `http://localhost:3000`. After a moment to load the iframe, the title should change and the location should read `http://localhost:3000/inner.html`, which is a default landing page I defined in `server.js`
  5. Click longer content and a similar thing should happen, changing the title and URL to reflect the navigation.
  6. Look at `server.js` for the definitions of the re-writing proxy and the wildcard handler that serves the outer frame w/ an appropriate `iframe src=...`
  7. Look at `public/js/inner.js` and `public/js/outer.js` to see how the message-passing works

## Caveats
 * The proxy & the message-passing should support query strings and subdirectories.
 * If the application links to anything apart from itself, the frame should be broken. You can see the google link on "long content" page does not load. Other pages will load but retain but the outer template. Labs accomplishes this by rewriting `<a href=$off-domain>` to include `target=_top`, but it does so by actually parsing the HTML into a DOM document rather than attempting via regex.
