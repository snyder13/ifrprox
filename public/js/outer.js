'use strict';
// listen to messages from the iframe. these are arbitrary, but in this example they're the path and title, which we use to update the location bar and, uh, title
//
// the title is just a polish thing, but updating the location is necessary to enable link-sharing, avoiding breaking refresh & back-forward
//
// setting the location directly would cause a redundant refresh, so we use the history api.
//
// if crappy old browser support is desired you'd set the location including the path in an anchor (http://foo/framelocation#/pass/this/url)
// that then needs client js support to add the anchor to the iframe src. I don't bother with labs
if (history) {
	var baseTitle = document.title;
	window[addEventListener ? 'addEventListener' : 'attachEvent'](addEventListener ? 'message' : 'onmessage', function(evt) {
		console.log(evt.data);
		document.title = baseTitle + ' - ' + evt.data.title
		var ma = evt.data.path.match(/^(.*?)proxy\/(.*)$/);
		if (ma) {
			history.pushState({}, document.title, ma[1] + ma[2]);
		}
	});
}
