'use strict';
// send path information to parent, so when people click around in the frame the outer window reflects their navigation in the location bar
// (should pass query string as well, but the example proxy doesn't forward it)
window[addEventListener ? 'addEventListener' : 'attachEvent'](addEventListener ? 'load' : 'onload', function() {
	parent.postMessage(
		{
			'title': document.title,
			'path': location.pathname
		},
		// must match parent window port and protocol. might need to be templated to support more possibilities or to support different hubs, https?://(www.)?hubhost.org
		'http://localhost:3000'
	);
});
