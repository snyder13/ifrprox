'use strict';
const
	express = require('express'), app = express(),
	request = require('request'),
	// html to inject "on-the-wire", so that anything served by this method can support the message-passing scheme between frames
	payload = `<script src="js/ie8.polyfils.min.js"></script><script src="js/inner.js"></script>`,
	outer = require('fs').readFileSync('frame.html', { 'encoding': 'utf8' });
	;

app.use(require('morgan')('combined'));
app.use('/static', express.static('public'));

// proxy to self -- kinda silly, but just for demo purposes
// needs more error-handling but weber already does that presumably
app.get(/^\/proxy\/(.+)/, (req, res, next) => {
	request(`http://localhost:3000/static/${req.params[0]}`, (err, resp) => {
		if (resp.headers['content-type'].indexOf('html') === -1) {
			return res.type(resp.headers['content-type']).send(resp.body);
		}
		res.type('html').send(resp.body.replace('<body>', '<body>' + payload));
	});
});

// serve frame, use path name to determine iframe location, so people can share URLs without having to realize it's framed
app.get(/^(\/.*)/, (req, res, next) => {
	res.type('html').send(outer.replace('$ifrUrl', 'proxy' + (req.params[0] === '/' ? '/inner.html' : req.params[0])));
});

app.listen(3000, () => {
	console.log(':3000')
})
